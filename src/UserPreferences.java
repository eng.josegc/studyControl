package src;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class UserPreferences extends JFrame implements WindowListener {
    private JButton logout;
    private Vector<JLabel> labels;
    private JTextField nameField;
    private JPasswordField passwordField;
    private JButton changeProfilePicture, backToMenu, save; 

    public UserPreferences() {
        super("User preferences");
        nameField = new JTextField();
        nameField.setText(User.getName());
        passwordField = new JPasswordField();
        passwordField.setText(User.getPassword());
        backToMenu = GuiButton.getBackButton(30, "Back");
        save = GuiButton.getSaveButton(30, "Save");
        setLayout(new GridBagLayout());
        methodController();
    }

    private void methodController() {
        initializeLabels();
        initializeButtons();
        setStylesForLabels();
        setButtonsStyles();
        setFieldsStyles();
        setActionForBackToMenu();
        placeComponents();
        launchGui();
    }
    
    private void setActionForBackToMenu() {
        backToMenu.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                        new Preferences();                              
                    }
                }
        );
    }

    private void initializeButtons() {
        changeProfilePicture = GuiButton.getAddButton(20, "Change picture");
    }

    private void setButtonsStyles() {
        changeProfilePicture.setOpaque(true);
        changeProfilePicture.setBackground(ColorPalette.getBase5());
        backToMenu.setOpaque(true);
        backToMenu.setBackground(ColorPalette.getBase5());
        save.setOpaque(true);
        save.setBackground(ColorPalette.getBase5());
    }

    private void setStylesForLabels() {
        for(int i = 1; i < labels.size(); i++) {
            labels.get(i).setFont(Fonts.getTextFontStyle());
        }
    }

    private void setFieldsStyles() {
        nameField.setFont(Fonts.getTittleFontStyle());
        passwordField.setFont(Fonts.getTittleFontStyle());
    }

    private void initializeLabels() {
        labels = new Vector<JLabel>();
        labels.add(User.getProfilePicture());
        labels.add(new JLabel("Name:"));
        labels.add(new JLabel("Password:"));
    }

    private void placeComponents() {
        // Profile picture
        add(
            labels.get(0),
            Constraints.setConstraints(0, 0, 1, 0, 2, 1, new int[]{0,0,15,0}, 'h')
        );

        add(
            changeProfilePicture,
            Constraints.setConstraints(0, 1, 1, 0, 2, 1, new int[]{0,0,15,0}, 'h')
        );

        add(
            labels.get(1),
            Constraints.setConstraints(0, 2, 1, 0, 2, 1, new int[]{0,0,0,0}, 'h')
        );

        add(
            nameField,
            Constraints.setConstraints(0, 3, 1, 0, 2, 1, new int[]{0,0,5,0}, 'h')
        );

        add(
            labels.get(2),
            Constraints.setConstraints(0, 4, 1, 0, 2, 1, new int[]{0,0,0,0}, 'h')
        );

        add(
            passwordField,
            Constraints.setConstraints(0, 5, 1, 0, 2, 1, new int[]{0,0,0,0}, 'h')
        );

        add(
            backToMenu,
            Constraints.setConstraints(0, 6, 1, 0, 1, 1, new int[]{15,0,0,0}, 'h')
        );

        add(
            save,
            Constraints.setConstraints(1, 6, 1, 0, 1, 1, new int[]{15,0,0,0}, 'h')
        );
    }


    private void launchGui() {
        setIconImage(AppIcon.getAppIcon());
        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public void windowClosing(WindowEvent arg0) {
        MainView.changeSubframeOpenedState(false);
    }

    public void windowOpened(WindowEvent arg0) {}
    public void windowClosed(WindowEvent arg0) {}
    public void windowIconified(WindowEvent arg0) {}
    public void windowDeiconified(WindowEvent arg0) {}
    public void windowActivated(WindowEvent arg0) {}
    public void windowDeactivated(WindowEvent arg0) {}
}
