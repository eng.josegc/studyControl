// Save, retrieve or delete the current session serialized
package src;
import java.io.*;

public class CurrentSession {
    private static String userSerializedFile = "currentUser.ser";
    private static User currentUser;

    // Checks if exist a serialized file
    public static boolean existSession() {
        try {
            FileInputStream fileInput = new FileInputStream(userSerializedFile);
            ObjectInputStream in = new ObjectInputStream(fileInput);
            in.close();
            return true;
        } catch(Exception e) {
            System.out.println("No session detected!");
        }       
        return false;
    } 

    // Create a serializable file to save the user information
    // and avoid ask for him credentials. Contains only his
    // name and password.
    public static void createSession(User user) {
        try {
           // output file
            FileOutputStream fileOut = new FileOutputStream("currentUser.ser");

            // stream
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(user);
            out.close();
            fileOut.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    // Used when already exists a session, reads the
    // serialized file.
    public static User retrieveSerializedUser() {
        try {
            FileInputStream inputFile = new FileInputStream (userSerializedFile);
            ObjectInputStream inputStream = new ObjectInputStream(inputFile);

            currentUser = (User)inputStream.readObject();
            inputStream.close();
        } catch(IOException e) {
            e.printStackTrace();
        } catch(ClassNotFoundException c) {
            System.out.println("User class not found.");
        }

        return currentUser;
    }

    // Used when user close his session, deletes
    // the serialized file.
    public static void deleteSession() {
        try {
            File fileToBeDeleted = new File("currentUser.ser");
            fileToBeDeleted.delete();
        } catch(Exception i) {
            i.printStackTrace();
        }
    }
}
