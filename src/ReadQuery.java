package src;
import java.util.*;
import java.sql.*;
import java.text.*;

public class ReadQuery {
    private static String url = "jdbc:mysql://localhost:3306/studyControl";
    private static String user = "root";
    private static String pass = "arzeus1998";

    public static int checkIfExistsUser(String name, String password) {
        int exists = 0;

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "SELECT EXISTS (SELECT * FROM user WHERE name in (?) AND password in (?))";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet result;
            // statement for query
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, name);
            pstmt.setString(2, password);
            
            // query
            result = pstmt.executeQuery();

            // result
            while(result.next()) {
                exists = result.getInt(1);
            }

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        } 
        return exists;
    } 

    /* 
        If is there active user in the data base, then this method
        get its information and creates and object that represents it.
        This assumes that exists one user with active values as 1.
     */
    public static void getUser() {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "SELECT * FROM user WHERE active = 1";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet result = pstmt.executeQuery();

            while(result.next()) {
                new User(
                        // Name
                        result.getString(1),

                        // Passsword
                        result.getString(2),

                        // Active session status
                        result.getInt(3),

                        // Profile picture
                        (result.getString(4) != "no_picture") ? 
                        "./pictures/boy.png" :
                        result.getString(4)
                );
            }

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    } 

    // Returns subject's names vector
    public static Vector retrieveUserSubjects(String fk_user) {
        Vector<Subject> temporalVector = new Vector<Subject>(); 

        try {
            Connection conn = DriverManager.getConnection(url, user, pass); 
            String query = "SELECT * FROM subject WHERE fk_user = (?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, fk_user);
            ResultSet result = pstmt.executeQuery();

            // Creates a subject only if it has reviews for today
            while(result.next()) {
                temporalVector.add(
                        new Subject(
                            result.getInt(1),
                            result.getString(2),
                            result.getString(3)
                        )
                );
            }
            
            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return temporalVector;
    }

    // Retrieves the reviews for today
    public static Vector<Topic> getTopics(int subject_id) {
        // Will store all reviews
        Vector<Topic> temporalVector = new Vector<Topic>();

        // For get review date individually
        int pk, fk, numberOfReviewIndex;
        String name;
        java.sql.Date sqlDate;
        java.util.Date date;

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = 
                "SELECT * " +
                "FROM review " +
                "WHERE fk_subject = ? ";
            PreparedStatement pstmt = conn.prepareStatement(query);

            pstmt.setInt(1, subject_id);
            ResultSet result = pstmt.executeQuery();

            while(result.next() != false) {
                pk = result.getInt(1);
                name = result.getString(2);
                sqlDate = result.getDate(3); // Time from db
                date = new java.util.Date(sqlDate.getTime()); // Converting time from db to Date of java
                fk = result.getInt(4);
                numberOfReviewIndex = result.getInt(5);
                
                // Creates new instance of review and add it to vector to be returned
                temporalVector.add(new Topic(pk, name, date, fk, numberOfReviewIndex));
            } 

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return temporalVector;
    }

    public static java.util.Date getNextReviewDate(int reviewId) {
        java.sql.Date sqlDate;
        java.util.Date date = null;

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "SELECT review_date FROM review WHERE review_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);

            pstmt.setInt(1, reviewId);
            ResultSet result = pstmt.executeQuery();

            while(result.next() != false) {
                sqlDate = result.getDate(1); // Time from db
                date = new java.util.Date(sqlDate.getTime()); // Converting time from db to Date of java
            } 

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return date;
    }

    public static int getNextNumberOfReview(int reviewId) {
        int nextNumberOfReview = 0;

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "SELECT number_of_review FROM review WHERE review_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);

            pstmt.setInt(1, reviewId);
            ResultSet result = pstmt.executeQuery();

            while(result.next() != false) {
                nextNumberOfReview = result.getInt(1); // Time from db
            } 

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return nextNumberOfReview;
    }

    public static int findUserActive() {
        int exists = 0;

        try {
            // statement for query
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "SELECT EXISTS(SELECT * FROM user WHERE active = 1)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet result;

            // query
            result = pstmt.executeQuery();

            // result
            while(result.next()) {
                exists = result.getInt(1);
            }

            pstmt.close();
            conn.close();
            result.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return exists;
    }
}
