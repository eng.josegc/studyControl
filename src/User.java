// User information
package src;
import java.util.Vector;
import java.io.*;
import java.awt.*;
import javax.swing.*;

public class User {
    private int active;
    private static String name;
    private static String password;
    private static Vector<Subject> subjects;
    private static Vector<String> subjectsNames;
    private static JLabel profilePicture, customPicture;
    private static String profilePicturePath;

    // constructor
    public User(String name, String password, int active, String profilePicturePath) {
        this.name = name;
        this.password = password;
        this.active = active;
        this.profilePicturePath = profilePicturePath;
        this.profilePicture = new JLabel(ResizeImage.createImage(profilePicturePath, 200, 200));

        /* Generates a vector with the subjects retrieved from db
        and return it. */
        subjects = new Vector<Subject>(ReadQuery.retrieveUserSubjects(this.getName()));
    }

    public static JLabel getProfilePicture() {
        return profilePicture;
    }

    public static JLabel getCustomProfilePicture() {
        customPicture = new JLabel(ResizeImage.createImage("./pictures/medium/boy.png", 40, 40));
        return customPicture;
    }

    public static int searchSubjectId(String subjectName) {
        for(Subject s : subjects) {
            if(s.getSubjectName() == subjectName) {
                return s.getId();
            }
        }

        return -1;
    }

    // Getters
    public int getActive() {
        return this.active;
    }

    public static String getName() {
        return name;
    }

    public static String getPassword() {
        return password;
    }

    public static Vector<Subject> getSubjects() {
        return subjects;
    }

    public static Vector<String> getSubjectNames() {
        Vector<String> names = new Vector<String>();
        for(Subject s : subjects) {
            names.add(s.getSubjectName());
        }

        return names;
    }

    // Setters
    public void setActive(int active) {
        this.active = active;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
