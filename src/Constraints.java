// Util for set a GridBagConstraints
package src;
import java.awt.*;

public class Constraints {
    public static GridBagConstraints c;

    /*
     *  Constructs a constraints and return it for use in other classes
     *  for place components in a GridBagLayout.
     *
     *  @param x Pointer to column
     *  @param y Pointer to row 
     *  @param wx Defines if this component can expand horizontally
     *  @param wy Defines if this component can expand vertically
     *  @param gw How many columns will use this component
     *  @param gh How many rows will use this component
     *  @param i[] External margin for this cell (top, left, bottom, right)
     *  @param f If the cell size is higher than the component is use for resize the
     *          component.
     * */
    public static GridBagConstraints setConstraints(int x, int y, int wx, int wy, int gw, int gh, int i[], char f) {
        c = new GridBagConstraints();

        c.gridx = x;
        c.gridy = y;

        c.weightx = wx;
        c.weighty = wy;

        c.gridwidth = gw; 
        c.gridheight = gh; 

        c.insets = new Insets(i[0], i[1], i[2], i[3]); 

        switch(f) {
            case 'h': 
                c.fill = GridBagConstraints.HORIZONTAL;
                break;
            case 'v':
                c.fill = GridBagConstraints.VERTICAL;
                break;
            case 'b':
                c.fill = GridBagConstraints.BOTH;
                break;
            default:
                c.fill = GridBagConstraints.NONE;
                break;
        }

        return c;
    }
}
