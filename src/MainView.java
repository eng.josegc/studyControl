package src;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Vector;

public class MainView implements ActionListener {
    private JFrame mainFrame;
    private JPanel reviewsPanel, homeworkAndProjectsPanel;
    private Vector<JLabel> labels;
    private JToolBar toolBar;
    private static JTabbedPane reviewsTab;
    private JTabbedPane homeworkAreaTab;
    private Vector<JButton> toolBarButtons;
    private static boolean subframeOpened, hasSubjects; // Avoid open more than one window
    private JLabel userToolBarData;
    private ImageIcon defaultSubjectIcon;
    
    // Constructor
    public MainView() {
        toolBar = new JToolBar(); // Add, edit, delete and preferences buttons
        toolBar.setFloatable(false);
        mainFrame = new JFrame("Study Control");
        reviewsTab = new JTabbedPane();
        toolBar = new JToolBar();
        // Review topics
        reviewsPanel = new JPanel();
        subframeOpened = false;

        defaultSubjectIcon = ResizeImage.createImage("./pictures/subject.png", 20, 20);

        // Flag for know if there is no one subject with topics for review
        hasSubjects = false;

        // Task to do for projects and homework
        homeworkAndProjectsPanel = new JPanel();

        callMethodsController();
    }

    // TODO: check order of calls 
    public void callMethodsController() {
        addStylesForJPanels();
        setLayouts();
        generateToolBarElements();
        setListeners();
        placeStudyAreaComponents();
        addPanelsToMainFrame();
        launchGraphicUserInterface();
    }

    public static void removeTab(String tabName) {
        reviewsTab.remove(reviewsTab.indexOfTab(tabName));
    }

    private void generateToolBarElements() {
        toolBarButtons = new Vector<JButton>();

        toolBarButtons.add(GuiButton.getAddButton(30, "New topic"));
        toolBarButtons.add(GuiButton.getAddSubject(30, "New subject"));
        toolBarButtons.add(GuiButton.getPreferencesButton(30, "Preferences"));

        for(int i = 0; i < toolBarButtons.size(); i++) {
            toolBar.add(toolBarButtons.get(i));
        }

        toolBar.addSeparator();
        toolBar.add(User.getCustomProfilePicture());
        toolBar.addSeparator();
        JLabel usernameToolbar = new JLabel(" Logged as: [ " + User.getName() + " ] ");
        usernameToolbar.setFont(Fonts.getTittleFontStyle());
        usernameToolbar.setOpaque(true);
        usernameToolbar.setBackground(ColorPalette.getBase4());
        usernameToolbar.setForeground(ColorPalette.getBase1());
        toolBar.add(usernameToolbar);
    } 

    private void setListeners() {
        for(JButton jb : toolBarButtons) {
            jb.addActionListener(this);
        }
    }

    public void addStylesForJPanels() {
        this.reviewsPanel.setBackground(ColorPalette.getBase3());
    }

    public void setLayouts() {
        mainFrame.setLayout(new BorderLayout());
        reviewsPanel.setLayout(new BorderLayout());
        this.addPanelsToMainFrame();
    }

    public void placeStudyAreaComponents() {
        // Iterates over vector of subjects from current user
        for(Subject s : User.getSubjects()) {
            if(!s.getTopics().isEmpty()) {
                if(!hasSubjects) {
                    hasSubjects = true;
                }

                // If has at least one topic, places in the frame
                reviewsTab.addTab(
                        s.getSubjectName(),
                        defaultSubjectIcon,
                        s.getPanel()
                );
            } 
        }         

        if(!hasSubjects) {
            JOptionPane.showMessageDialog(
                    mainFrame,
                    "Take a break, you don't have reviews for today.", "Take a break", JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    public void addPanelsToMainFrame() {
        // tabs
        toolBar.setOrientation(0);
        toolBar.setFloatable(false);
        mainFrame.add(toolBar, BorderLayout.NORTH);
        reviewsPanel.add(reviewsTab);        
        mainFrame.add(reviewsPanel);
        this.launchGraphicUserInterface();
    }

    public static void changeSubframeOpenedState(boolean status) {
        subframeOpened = status;
    }

    public void launchGraphicUserInterface() {
        mainFrame.setIconImage(AppIcon.getAppIcon());
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
        mainFrame.setSize(screenSize.width, screenSize.height);
        mainFrame.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        // Preferences button
        if(e.getSource() == toolBarButtons.get(2) && subframeOpened == false) {
            changeSubframeOpenedState(true);
            new Preferences();
        } else if(e.getSource() == toolBarButtons.get(0) && subframeOpened == false) {
            changeSubframeOpenedState(true);
            new AddNewTopic("Add new topic");
        }
    }
}
