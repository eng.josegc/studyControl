package src;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class EditTopic extends AddNewTopic {
    protected Topic topic;

    public EditTopic(int indexOfTopic, Topic topic) {
        super("Edit topic information");

        // Removing unnecessary components inherit from superclass
        remove(super.newSubjectName);
        remove(super.labels.get(2));
        revalidate();
        repaint();

        // Overrides the text for this label
        super.labels.get(4).setText("Next review date: ");

        this.topic = topic;

        methodController();
    }

    private void methodController() {
        setNewTextForLabels();
        setDefaults();
        getSubjectName();
    }

    private void getSubjectName() {
        topic.getSubjectId();
    }

    // Sets defaults values from each topic
    private void setDefaults() {
        // Default name of topic ---------------------------------------- 
        super.topicName.setText(
                topic.getReviewName()
        );

        // Default subject owner ---------------------------------------- 
        String nameOfSubject = "";
        for(Subject subject : User.getSubjects()) {
            // When the subject owner of this topic matchs, get its name
            if(topic.getSubjectId() == subject.getId()) {
                nameOfSubject = subject.getSubjectName();
            }
        }

        // Finds the position of subject in combo and set as default
        for(int i = 0; i < super.subjects.getItemCount(); i++) {
            if(super.subjects.getItemAt(i) == nameOfSubject) {
                super.subjects.setSelectedItem(
                        super.subjects.getItemAt(i)
                );
            }
        }

        // Default stage of study ---------------------------------------
        super.stage.setSelectedItem(
                super.stage.getItemAt(topic.getNumberOfReviewIndex())
        );

        // Default date of next review ---------------------------------------
        // Converts the date to calendar object for get individuals values (day, month and year)
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(topic.getDateReview());
        super.day.setSelectedItem(calendar.get(Calendar.DAY_OF_MONTH));
        super.month.setSelectedItem(calendar.get(Calendar.MONTH) + 1);
        super.year.setSelectedItem(calendar.get(Calendar.YEAR));
    }

    private void setNewTextForLabels() {
        labels.get(0).setText("Name of topic: ");
    } 

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
