// Provides the Image that contains
package src;
import java.awt.*;
import javax.swing.*;

public class AppIcon {
    private static ImageIcon appIcon;

    public static Image getAppIcon() {
        appIcon = new ImageIcon("./pictures/logo.png");
        return appIcon.getImage();
    }
}
