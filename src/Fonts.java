package src;
import java.awt.*;
import java.io.*;

public class Fonts {
    /*
    public static Font getTittleFontStyle() {
        try {
            // Creates font usign a file
            Font input = Font.createFont(Font.TRUETYPE_FONT, new File("./fonts/inconsolata.ttf")).deriveFont(Font.BOLD, 24f);   

            // Collection of fonts for this system
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

            // Load the font created on the system 
            ge.registerFont(input);

            return input;
        } catch (IOException|FontFormatException e) {
            e.printStackTrace();
        }

        return null;
    }*/

    public static Font getTittleFontStyle() {
        return new Font("Arial", Font.BOLD, 24);
    }

    public static Font getDescriptionFontStyle() {
        return new Font("Arial", Font.BOLD, 18);
    }

    public static Font getTextFontStyle() {
        return new Font("Arial", Font.PLAIN, 18);
    }
    
    /*
    public static Font getTextFontStyle() {
        try {
            // Creates font usign a file
            Font input = Font.createFont(Font.TRUETYPE_FONT, new File("./fonts/inconsolata.ttf")).deriveFont(18f);   

            // Collection of fonts for this system
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

            // Load the font created on the system 
            ge.registerFont(input);

            return input;
        } catch (IOException|FontFormatException e) {
            e.printStackTrace();
        }

        return null;
    } */
}
