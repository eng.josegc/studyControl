package src;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;

public class ResizeImage {
    public static BufferedImage resize(BufferedImage image, int height, int width) {

        // Generates A scale version of the image
        Image temporal = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        
        // 8 bits representation in a package of integer
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        // the bits that representate the image are draw in g2d object
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(temporal, 0, 0, null);
        g2d.dispose();
        return resized;
    }

    public static ImageIcon createImage(String imgPath, int height, int width) {
        try {
            File fileInput = new File(imgPath);
            BufferedImage image = ImageIO.read(fileInput);
            BufferedImage resized = resize(image, height, width);
            File fileOutput = new File(imgPath + "_resized.png");
            ImageIO.write(resized, "png", fileOutput);
            ImageIcon imageResized = new ImageIcon(fileOutput.getPath());
            return imageResized;
        } catch(IOException i) {
            i.printStackTrace();
        }
        return null;
    }
}
