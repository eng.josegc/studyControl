import java.awt.*;
import javax.swing.*;

public class ToolBarImages {
    JFrame frame;
    JToolBar toolBar;
    String images[];
    String labels[];
    ImageIcon[] icons;
    JButton[] buttons;

    public ToolBarImages() {
        frame = new JFrame();
        
        toolBar = new JToolBar();
        images = new String[]{"./pictures/add.png", "./pictures/edit.png", "./pictures/delete.png"}; 
        labels = new String[]{"New", "Edit", "Delete"};
        icons = new ImageIcon[images.length];
        buttons = new JButton[images.length];

        frame.setLayout(new BorderLayout());

        for(int i = 0; i < labels.length; i++) {
            icons[i] = new ImageIcon((images[i]));
            buttons[i] = new JButton(icons[i]);
            buttons[i].setToolTipText(labels[i]);
            toolBar.add(buttons[i]);
        }

        frame.add(toolBar, BorderLayout.NORTH);
        frame.setLocationRelativeTo(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new ToolBarImages();
    }
}
