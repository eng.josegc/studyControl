package src;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Preferences extends JFrame implements WindowListener {
    Vector <JButton> buttons;

    public Preferences() {
        super("Preferences");
        addWindowListener(this);
        setLayout(new GridBagLayout());
        methodsController();
    }

    private void methodsController() {
        initializeButtons();
        setStylesForButtons();
        placeComponents();
        setManageSubjectButtonAction();
        setUserPreferencesAction();
        launchGui();
    }

    private void setStylesForButtons() {
        for(JButton b : buttons) {
            b.setBackground(ColorPalette.getBase5());
            b.setFont(Fonts.getDescriptionFontStyle());
        }
    }

    private void initializeButtons() {
        buttons = new Vector<JButton>();
        buttons.add(GuiButton.getOptionButton(25, "Manage subjects"));
        buttons.add(GuiButton.getOptionButton(25, "Account"));
    }

    private void setManageSubjectButtonAction() {
        // Manage subjects window
        (buttons.get(0)).addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                        new ManageSubject();
                    }
                }
        );

    }

    private void setUserPreferencesAction() {
        // User preferences window
        buttons.get(1).addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                        new UserPreferences();
                    }
                }
        );
    }

    private void placeComponents() {
        add(
            buttons.get(0),
            Constraints.setConstraints(0, 0, 1, 0, 1, 1, new int[]{0,0,0,0}, 'h')
        );

        add(
            buttons.get(1),
            Constraints.setConstraints(0, 1, 1, 0, 1, 1, new int[]{10,0,0,0}, 'h')
        );
    }

    private void launchGui() {
        setSize(600, 200);
        setIconImage(AppIcon.getAppIcon());
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public void windowClosing(WindowEvent arg0) {
        MainView.changeSubframeOpenedState(false);
    }

    public void windowOpened(WindowEvent arg0) {}
    public void windowClosed(WindowEvent arg0) {}
    public void windowIconified(WindowEvent arg0) {}
    public void windowDeiconified(WindowEvent arg0) {}
    public void windowActivated(WindowEvent arg0) {}
    public void windowDeactivated(WindowEvent arg0) {}
}
