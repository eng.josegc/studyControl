package src;
import java.sql.*;
import java.util.*;
import java.text.*;

public class CreateQuery {
    // db access
    private static String url = "jdbc:mysql://localhost:3306/studyControl";
    private static String user = "root";
    private static String pass = "arzeus1998";

    // New topic of existing subject without more options
    public static void insertNewTopic(String topicName, int fkSubject, java.util.Date date) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String newTopic = "INSERT INTO review (name, fk_subject, review_date)" +
                "VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(newTopic);
            pstmt.setString(1, topicName);
            pstmt.setInt(2, fkSubject);
            
            // Date to sql date
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            pstmt.setDate(3, sqlDate);
            pstmt.execute();

            // Closing connections
            pstmt.close();
            conn.close();
        } catch(Exception e) {
            System.out.println("Exception at insertNewTopic(): ");
            e.printStackTrace();
        }
    }

    // New topic and new subject without more options
    public static void insertNewTopic(
            String topicName,
            String newSubjectName,
            String fkUser
            ) {
        try {
            // First, register the new subject
            Connection conn = DriverManager.getConnection(url, user, pass);
            String newSubjectQuery =
                "INSERT INTO subject(name, fk_user)" +
                "VALUES(?, ?)";

            PreparedStatement pstmt = conn.prepareStatement(newSubjectQuery);
            pstmt.setString(1, newSubjectName);
            pstmt.setString(2, fkUser);
            pstmt.execute();
            conn.close();
            pstmt.close();

            // Next, get id of the new subject 
            conn = DriverManager.getConnection(url, user, pass);
            String getNewSubjectId = 
                "SELECT subject_id " +
                "FROM subject " +
                "WHERE name = (?)";
            pstmt = conn.prepareStatement(getNewSubjectId);
            pstmt.setString(1, newSubjectName);
            ResultSet rs = pstmt.executeQuery();
            int newSubjectId = 0;

            while(rs.next()) {
                newSubjectId = rs.getInt(1);
            }

            conn.close();
            rs.close();
            pstmt.close();

            // Finally, register the new topic
            conn = DriverManager.getConnection(url, user, pass);
            String newTopic = 
                "INSERT INTO review(name, fk_subject)" +
                "VALUES(?, ?)";
            pstmt = conn.prepareStatement(newTopic);
            pstmt.setString(1, topicName);
            pstmt.setInt(2, newSubjectId);
            pstmt.execute();
            pstmt.close();
            conn.close();
        } catch(Exception e) {
            System.out.println("EXCEPTION -> insertNewTopicAndNewSubject():");
            e.printStackTrace();
        }
    }

    // New topic of existing subject with more options
    public static void insertNewTopic(
            String topicName, 
            java.util.Date lastStudyDate,
            int fkSubject, 
            int studyStage 
            ) {
        // Date to sql date
        java.sql.Date sqlDate = new java.sql.Date(lastStudyDate.getTime());

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query =
                "INSERT INTO review(name, review_date, fk_subject, number_of_review)" +
                "VALUES(?, ?, ?, ?)";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, topicName);
            pstmt.setDate(2, sqlDate);
            pstmt.setInt(3, fkSubject);
            pstmt.setInt(4, studyStage);

            pstmt.execute();

            conn.close();
            pstmt.close();
        } catch(Exception e) {
            System.out.println("EXCEPTION -> insertNewTopic(String, Date, int, int) case 3:");
            e.printStackTrace();
        }
    }

    public static void insertNewTopic(
            String newTopicName,
            String newSubjectName,
            String fkUser,
            int studyStage,
            java.util.Date lastStudyDate
            ) {
        try {
            // First, register the new subject --------------------------------------
            Connection conn = DriverManager.getConnection(url, user, pass);
            String newSubjectQuery =
                "INSERT INTO subject(name, fk_user)" +
                "VALUES(?, ?)";

            PreparedStatement pstmt = conn.prepareStatement(newSubjectQuery);
            pstmt.setString(1, newSubjectName);
            pstmt.setString(2, fkUser);

            pstmt.execute();
            pstmt.close();

            // TODO: replace this step with store procedure for get id after insert
            // Next, get id of the new subject --------------------------------------
            String getNewSubjectId = 
                "SELECT subject_id " +
                "FROM subject " +
                "WHERE name = (?)";
            pstmt = conn.prepareStatement(getNewSubjectId);
            pstmt.setString(1, newSubjectName);
            ResultSet rs = pstmt.executeQuery();
            int newSubjectId = 0;

            while(rs.next()) {
                newSubjectId = rs.getInt(1);
            }

            rs.close();
            pstmt.close();

            // Finally, register the new topic ---------------------------------------
            // Date to sql date
            java.sql.Date sqlDate = new java.sql.Date(lastStudyDate.getTime());

            String query =
                "INSERT INTO review(name, review_date, fk_subject, number_of_review)" +
                "VALUES(?, ?, ?, ?)";

            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, newTopicName);
            pstmt.setDate(2, sqlDate);
            pstmt.setInt(3, newSubjectId);
            pstmt.setInt(4, studyStage);

            pstmt.execute();

            conn.close();
            pstmt.close();
        } catch(Exception e) {
            System.out.println("EXCEPTION -> insertNewTopicAndNewSubject() for case 4:");
            e.printStackTrace();
        }
    }
}
