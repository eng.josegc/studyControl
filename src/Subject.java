/*
 *  Creates a panel with information of topics
 *  and return it to MainView.
 * */

package src;
import java.util.Vector;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Subject implements ActionListener {
    // Basic info
    private String name, userOwner;
    private int id;
    protected Vector<Topic> topics; // set of name, number and status of topics
    private JPanel subjectPanel; // Panel to be placed in main view
    private static Vector<JLabel> columnNames; // Names that describe each column
    private Vector<JButton> completeButtons, editButtons, deleteButtons;
    private int indexButton; // Which action button is pressed (complete, edit or delete);
    private JScrollPane scroll;
    private Vector<Component> components;

    public Subject(int id, String name, String userOwner) {
        this.id = id;
        this.name = name;
        this.userOwner = userOwner;

        components = new Vector<Component>();
        // Main panel to be returned
        subjectPanel = new JPanel();
        topics = new Vector<Topic>();
        columnNames = new Vector<JLabel>();
        scroll = new JScrollPane(subjectPanel);
        // Retrieves a vector of topics from db
        topics = new Vector<Topic>(ReadQuery.getTopics(id)); 

        completeButtons = new Vector<JButton>(topics.size());    
        editButtons = new Vector<JButton>(topics.size());    
        deleteButtons = new Vector<JButton>(topics.size());    

        for(int i = 0; i < topics.size(); i++) {
            // Initialize the jbuttons
            completeButtons.add(GuiButton.getCompleteButton(15, "Complete"));
            editButtons.add(GuiButton.getEditButton(15, "Edit"));
            deleteButtons.add(GuiButton.getDeleteButton(15, "Delete"));
        }


        methodCallsController();
    }

    public void methodCallsController() {
        setColumnNames();
        setLayouts();
        setLabelStyles();
        placeReviewComponents();
        setActionButtonsListeners();
        setStylesForJPanel();
    }

    public void setLayouts() {
        subjectPanel.setLayout(new GridBagLayout());
    }

    public void setColumnNames() {
        columnNames.add(new JLabel("Name"));
        columnNames.add(new JLabel("Status"));
        columnNames.add(new JLabel("Stage"));
        columnNames.add(new JLabel("Next review date"));
        columnNames.add(new JLabel("Complete"));
        columnNames.add(new JLabel("Edit"));
        columnNames.add(new JLabel("Delete"));
    }

    // Set styles for the name of topics
    public void setLabelStyles() {
        // Name of topic and number of review
        for(Topic topic : topics) {
          topic.getLabelName().setFont(Fonts.getTextFontStyle());
          topic.getLabelName().setForeground(ColorPalette.getBase3());
        }

        // Column names
        for(JLabel label : columnNames) {
            label.setHorizontalAlignment(JLabel.CENTER);
            label.setFont(Fonts.getTittleFontStyle());
            label.setForeground(ColorPalette.getBase1());
            label.setOpaque(true);
            label.setBackground(ColorPalette.getBase3());
        }
    }

    public void placeReviewComponents() {
        // COLUMN DESCRIPTION (0,0)
        for(int i = 0; i < columnNames.size(); i++) {
            subjectPanel.add(
                    columnNames.get(i),
                    Constraints.setConstraints(i, 0, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                    );
        }

        // topics NAME (0, 2)
        for(int y = 1, i = 0; i < topics.size(); y++, i++) {
            // Evaluates if this topics is overdue or active for today
            if(DateUtils.getToday().compareTo(topics.get(i).getDateReview()) >= 0) {
                // Topic name
                subjectPanel.add(
                        topics.get(i).getLabelName(),
                        Constraints.setConstraints(0, y, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                        );

                // Status label 
                subjectPanel.add(
                        topics.get(i).getStatusLabel(false), 
                        Constraints.setConstraints(1, y, 0, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                        );

                // Number of review label
                subjectPanel.add(
                        topics.get(i).getNumberOfReviewLabel(null), 
                        Constraints.setConstraints(2, y, 1, 1, 1, 1, new int[]{0,0,10,0}, 'h')
                        );

                // Next review date
                subjectPanel.add(
                        topics.get(i).getNextReviewDateLabel(null), 
                        Constraints.setConstraints(3, y, 1, 1, 1, 1, new int[]{0,0,10,0}, 'h')
                        );
                
                subjectPanel.add(
                        completeButtons.get(i),
                        Constraints.setConstraints(4, y, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                        );

                subjectPanel.add(
                        editButtons.get(i),
                        Constraints.setConstraints(5, y, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                        );

                subjectPanel.add(
                        deleteButtons.get(i),
                        Constraints.setConstraints(6, y, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                        );
            } 
        }
    }

    // Listeners for complete, edit and delete buttons for review actions
    private void setActionButtonsListeners() {
        for(int i = 0; i < completeButtons.size(); i++) {
            completeButtons.get(i).addActionListener(this);
            editButtons.get(i).addActionListener(this);
            deleteButtons.get(i).addActionListener(this);
        }
    }

    public void setStylesForJPanel() {
        subjectPanel.setBackground(ColorPalette.getBase3());
    }

    public String getSubjectName() {
        return name;
    }

    // returns the constructed panel for place in main view 
    public JScrollPane getPanel() {
        return scroll;     
    }

    public int getId() {
        return id;
    }

    public Vector<Topic> getTopics() {
        return topics;
    }

    public void actionPerformed(ActionEvent e) {
        // Checks if the button pressed was a completed button
        if(completeButtons.contains(e.getSource())) {
            indexButton = completeButtons.indexOf(e.getSource()); // Save button pressed index

            // Gets the current date for this review and calculate the next date
            if((topics.get(indexButton).getDateReview()).compareTo(DateUtils.getToday()) < 0) {
                Query.updateDateReview(
                    DateUtils.calculateNextReview(
                            DateUtils.getToday(),
                            (topics.get(indexButton)).getNumberOfReviewIndex()
                    ),
                   (topics.get(indexButton)).getId()
                );
            } else {
                // DB update
                Query.updateDateReview(
                    DateUtils.calculateNextReview(
                            (topics.get(indexButton)).getDateReview(),
                            (topics.get(indexButton)).getNumberOfReviewIndex()
                    ),
                   (topics.get(indexButton)).getId()
                );
            }

            // Increments the value of how many times this topic has been studied
            Query.updateNumberOfReview(
                    (topics.get(indexButton)).getId(),
                    ((topics.get(indexButton)).getNumberOfReviewIndex())
            );

            components.add(topics.get(indexButton).getLabelName());
            components.add(topics.get(indexButton).getStatusLabel());
            components.add(topics.get(indexButton).getNumberOfReviewLabel("remove"));
            components.add(topics.get(indexButton).getNextReviewDateLabel("remove"));
            components.add(completeButtons.get(indexButton));
            components.add(editButtons.get(indexButton));
            components.add(deleteButtons.get(indexButton));

            removeGraphicComponent(components);
            
            // Cleans the vector
            components.clear();
        } else if(editButtons.contains(e.getSource())) {
            indexButton = editButtons.indexOf(e.getSource()); // Save button pressed index
            new EditTopic(indexButton, topics.get(indexButton));
        } else {
            indexButton = deleteButtons.indexOf(e.getSource()); // Save button pressed index
            
            String dialogOptions[] = {"Yes", "No"}; 

            int confirmation = JOptionPane.showOptionDialog(
                subjectPanel,
                "Are you sure to delete this topic?",
                "Delete topic",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                dialogOptions,
                dialogOptions[1]
            );

            if(confirmation == 0) {
                // Delete from database
                DeleteQuery.deleteTopic(
                    (topics.get(indexButton)).getId()
                    );


                // Adds the components to its vector
                components.add(topics.get(indexButton).getLabelName());
                components.add(topics.get(indexButton).getStatusLabel());
                components.add(topics.get(indexButton).getNumberOfReviewLabel("remove"));
                components.add(topics.get(indexButton).getNextReviewDateLabel("remove"));
                components.add(completeButtons.get(indexButton));
                components.add(editButtons.get(indexButton));
                components.add(deleteButtons.get(indexButton));

                removeGraphicComponent(components);
                
                // Cleans the vector
                components.clear();
            }
        }
    }

    private void removeGraphicComponent(Vector<Component> comp) {
        for(Component c : comp) {
            subjectPanel.remove(c);
            subjectPanel.revalidate();
            subjectPanel.repaint();
        }
    }
}
