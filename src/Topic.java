package src;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.text.*;

public class Topic {
    protected int id, fk;
    private String name, subject;
    private java.util.Date date;
    private int numberOfReviewIndex;

    private static Vector<String> numberOfReview;     
    private JLabel done;
    private JLabel labelName;
    private JLabel dateLabel;
    private JLabel numberOfReviewLabel;   

    // Constructor
    public Topic(int id, String name, Date date, int fk, int numberOfReviewIndex) {
        // Basic info
        this.id = id;
        this.name = name;
        this.date = date;
        this.date = DateUtils.setStandardFormat(this.date);
        this.fk = fk;
        this.numberOfReviewIndex = numberOfReviewIndex;
        

        this.labelName = new JLabel(name);
        fillNumberOfReview();
    }

    public int getSubjectId() {
        return fk;
    }

    // Constructor for case 1
    public Topic(String name, int fk) {
        this.name = name;
        this.fk = fk;
    }

    // Vector for show stage of study
    public static void fillNumberOfReview() {
        numberOfReview = new Vector<String>();
        numberOfReview.add("Learned for the first time");
        numberOfReview.add("24 hours");
        numberOfReview.add("48 hours");
        numberOfReview.add("1 week");
        numberOfReview.add("2 weeks");
        numberOfReview.add("1 month");
        numberOfReview.add("2 months");
        numberOfReview.add("4 months");
        numberOfReview.add("6 months");
        numberOfReview.add("Review optional");
    }

    public static Vector<String> getStagesOfStudy() {
        return numberOfReview;
    }

    // To do after press complete button
    public JLabel getNextReviewDateLabel(String action) {
        if(action == "remove") {
            return dateLabel;
        } else {
            if(DateUtils.getToday().compareTo(date) == 0) {
                dateLabel = new JLabel("Today");
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dateLabel = new JLabel(sdf.format(date.getTime()));
            }

            dateLabel.setHorizontalAlignment(JLabel.CENTER);
            dateLabel.setFont(Fonts.getTextFontStyle());
            dateLabel.setOpaque(true);
            if(DateUtils.getToday().compareTo(date) > 0) {
                dateLabel.setForeground(ColorPalette.getBase1());
                dateLabel.setBackground(ColorPalette.getBase4());
            } else {
                dateLabel.setForeground(ColorPalette.getBase3());
                dateLabel.setBackground(ColorPalette.getBase2());
            }
            
            return dateLabel;
        }
    }

    public JLabel getNumberOfReviewLabel(String action) {
        // After press delete button
        if(action == "remove") {
            return numberOfReviewLabel;
        } else {
            numberOfReviewLabel = new JLabel(numberOfReview.get(numberOfReviewIndex));
            numberOfReviewLabel.setHorizontalAlignment(JLabel.CENTER);
            numberOfReviewLabel.setFont(Fonts.getTextFontStyle());
            numberOfReviewLabel.setOpaque(true);
            if(DateUtils.getToday().compareTo(date) > 0) {
                numberOfReviewLabel.setBackground(ColorPalette.getBase4());
                numberOfReviewLabel.setForeground(ColorPalette.getBase1());
            } else {
                numberOfReviewLabel.setBackground(ColorPalette.getBase2());
                numberOfReviewLabel.setForeground(ColorPalette.getBase3());
            }

            return numberOfReviewLabel;
        }
    }

    public JLabel getLabelName() {
        labelName.setHorizontalAlignment(JLabel.CENTER);
        labelName.setFont(Fonts.getTextFontStyle());
        labelName.setOpaque(true);
        if(DateUtils.getToday().compareTo(date) > 0) {
            labelName.setBackground(ColorPalette.getBase4());
            labelName.setForeground(ColorPalette.getBase1());
        } else {
            labelName.setBackground(ColorPalette.getBase2());
            labelName.setForeground(ColorPalette.getBase3());
        }
        return this.labelName;
    }

    /**
     *  Returns a label with the status for this review,
     *  it can has two states: reviewed and unreviewed, the label
     *  to be returned depends of status parameter.
     *
     *  @param status Defines what kind of label will return, false returns unreviewed
     *  label and true returns reviewed label.
     * */
    public JLabel getStatusLabel(boolean status) {
        done = new JLabel(ResizeImage.createImage("./pictures/unreviewed.png", 22, 22));
        done.setToolTipText("This topic hasn't be studied");
        done.setOpaque(true);
        if(DateUtils.getToday().compareTo(date) > 0) {
            done.setBackground(ColorPalette.getBase4());
        } else {
            done.setBackground(ColorPalette.getBase2());
        }
        return done;
    }

    public JLabel getStatusLabel() {
        return done;
    }

    /**
     *  Changes the image showing in subject panel.
     *
     *  @param icon The new image to show.
     * */
/*     public void changeStatusLabelIcon(ImageIcon icon) {
        this.done.setIcon(icon);
        this.done.setOpaque(true);
        this.done.setToolTipText("Topic studied");
        this.done.setBackground(ColorPalette.getGreen());
    } */

    /**
     *  Change the data showing in subject panel about the next
     *  date of review.
     * */
/*     public void changeNextReviewDateLabelStyle() {
        dateLabel.setBackground(ColorPalette.getGreen());
    }

    public void changeNumberOfReviewLabelStyle() {
        numberOfReviewLabel.setBackground(ColorPalette.getGreen());
    }

    public void changeNameOfReviewLabelStyle() {
        labelName.setBackground(ColorPalette.getGreen());
    }

    public void refreshNextReviewDateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        this.dateLabel.setText(
                sdf.format(ReadQuery.getNextReviewDate(id))
        );
    }

    public void refreshNumberOfReviewLabel() {
        this.numberOfReviewLabel.setText(
                numberOfReview.get(ReadQuery.getNextNumberOfReview(id))
        );
    }
 */
    public String getReviewName() {
        return name; 
    }    
    
    public int getNumberOfReviewIndex() {
        return numberOfReviewIndex;
    }

    public int getId() {
        return id;
    }

    public Date getDateReview() {
        return date; 
    }
}

