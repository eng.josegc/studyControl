package src;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// Graphic dialog for login or register
public class Dialogs implements ActionListener {
    private static JPanel panel;
    private static JFrame frame;
    private static JButton firstButton, secondButton;
    private static GridBagConstraints gbc;
    private static JLabel usernameLabel,
            passwordLabel,
            picture;
    private static JPasswordField passwordField;
    private static JTextField usernameField;
    private static String typeOfForm;
    private static boolean succeeded;
    private static User activeUser;

    // Constructor
    public void showDialog(String frameName) {
        frame = new JFrame(frameName);
        typeOfForm = (frameName == "Login") ? "login" : "signup";
        panel = new JPanel(new GridBagLayout());
        gbc = new GridBagConstraints();
        usernameLabel = new JLabel("Ingress your name:");
        passwordLabel = new JLabel("Type your password:");
        usernameField = new JTextField();
        passwordField = new JPasswordField();
        this.firstButton = new JButton((frameName == "Login") ? "Login \u2192" : "Create account \u2192");
        this.secondButton = new JButton((frameName == "Login") ? "Create account" : "Back to login \u2190");
        picture = new JLabel(ResizeImage.createImage((frameName == "Login") ? "./pictures/login.png" : "./pictures/signup.png", 100, 100));

        methodController();
    }

    public void placeComponents() {
        // PICTURE CONSTRAINTS
        panel.add(picture, Constraints.setConstraints(0,0,1,0,2,1,new int[]{0,0,10,0}, 'h')); 

        // USERNAME LABEL FIELD CONSTRAINTS
        panel.add(usernameLabel, Constraints.setConstraints(0,1,1,0,2,1,new int[]{0,0,5,0}, 'h')); 

        // USER NAME FIELD CONSTRAINTS
        panel.add(usernameField, Constraints.setConstraints(0,2,1,0,2,1,new int[]{0,0,10,0}, 'h')); 

        // PASSWORD LABEL CONSTRAINTS 
        panel.add(passwordLabel, Constraints.setConstraints(0,3,1,0,2,1,new int[]{0,0,5,0}, 'h')); 

        // PASSWORD FIELD CONSTRAINTS 
        panel.add(passwordField, Constraints.setConstraints(0,4,1,0,2,1,new int[]{0,0,10,0}, 'h')); 

        firstButton.setBackground(ColorPalette.getBase5());
        firstButton.setForeground(ColorPalette.getBase1());
        panel.add(firstButton, Constraints.setConstraints(0,5,1,0,2,1,new int[]{0,0,0,0}, 'h')); 

        // SIGN UP BUTTON CONSTRAINTS 
        secondButton.setBackground(ColorPalette.getBase5());
        secondButton.setForeground(ColorPalette.getBase1());
        panel.add(secondButton, Constraints.setConstraints(1,6,1,0,1,1,new int[]{15,0,0,0}, 'h')); 
    }

    public void methodController() {
        placeComponents();
        setTextStyles();
        setListeners();
        launchGUI();
    }

    public void setListeners() {
        firstButton.addActionListener(this);
        secondButton.addActionListener(this);
    }

    // Set the font family, color and sizes to labels
    public void setTextStyles() {
        usernameLabel.setFont(Fonts.getTextFontStyle());
        passwordLabel.setFont(Fonts.getTextFontStyle());
        usernameField.setFont(Fonts.getTittleFontStyle());
        passwordField.setFont(Fonts.getTittleFontStyle());
        firstButton.setFont(Fonts.getTittleFontStyle());
        secondButton.setFont(Fonts.getTextFontStyle());
    }

    public void launchGUI() {
        frame.add(panel);
        frame.setIconImage(AppIcon.getAppIcon());
        frame.setSize(400, 400);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        // If the window is a login form
        if(typeOfForm == "login") {
            if(e.getSource() == secondButton) {
                // When sign up button is pressed close this window and show a sign up frame
                frame.dispose();
                showDialog("Sign up");
            } else if(e.getSource() == firstButton) {
                if(usernameField.getText().equals("")) {
                        JOptionPane.showMessageDialog(frame, "Please, ingress your username", "Error", JOptionPane.ERROR_MESSAGE);
                    } else if(String.valueOf(passwordField.getPassword()).equals("")) {
                        JOptionPane.showMessageDialog(frame, "Please, ingress your password", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        // When user press login button calls the auth util from Login class
                        if(Login.authenticate(getUsername(), getPassword())) {
                            // Exist user in db
                            succeeded = true;

                            // Then set active attribute
                            Query.changeActiveSession(getUsername(), 1);
                            ReadQuery.getUser();
                            frame.dispose();
                            
                            // Shows the mainview with the user information retrieved
                            new MainView();
                        } else {
                            JOptionPane.showMessageDialog(frame, "Invalid username or password", "Invalid credentials", JOptionPane.ERROR_MESSAGE);
                            // Then reset username and password values
                            usernameField.setText("");
                            passwordField.setText("");
                            succeeded = false;
                        }
                    }
                 
            } 
        } else if(typeOfForm == "signup"){
            // Otherwise, prepare actions for sign up window
                if(e.getSource() == secondButton) {
                    frame.dispose();
                    showDialog("Login");
                } else if(e.getSource() == firstButton) {
                    // Query for insert new user
                    if(usernameField.getText().equals("")) {
                        JOptionPane.showMessageDialog(frame, "Please, ingress your username", "Error", JOptionPane.ERROR_MESSAGE);
                    } else if(String.valueOf(passwordField.getPassword()).equals("")) {
                        JOptionPane.showMessageDialog(frame, "Please, ingress your password", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        Register.newUser(getUsername(), getPassword(), 1);
                        User temporalUser = new User(getUsername(), getPassword(), 1, "no_picture");
                        JOptionPane.showMessageDialog(frame, "Registered success", "Sign up", JOptionPane.INFORMATION_MESSAGE);
                        CurrentSession.createSession(temporalUser);
                        frame.dispose();
                        new MainView();
                    }
                    
                }
        }
    }

    public String getUsername() { 
        return usernameField.getText().trim();
    }

    public String getPassword() {
        return String.valueOf(passwordField.getPassword());
    }

    public boolean isSucceeded() {
        return succeeded;
    }
}
