package src;
import java.awt.*;

public class ColorPalette {
    // Set 1
    private static final Color base1 = new Color(220, 220, 221);
    private static final Color base2 = new Color(197, 195, 198);
    private static final Color base3 = new Color(70, 73, 76);
    private static final Color base4 = new Color(76, 92, 104);
    private static final Color base5 = new Color(25, 133, 161);

/*     private static final Color base1 = new Color(220, 220, 221);
    private static final Color base2 = new Color(185, 255, 183);
    private static final Color base3 = new Color(70, 73, 76);
    private static final Color base4 = new Color(252, 171, 138);
    private static final Color base5 = new Color(158, 237, 213); */

    public static Color getBase1() {
        return base1;
    }

    public static Color getBase2() {
        return base2;
    }

    public static Color getBase3() {
        return base3;
    }

    public static Color getBase4() {
        return base4;
    }

    public static Color getBase5() {
        return base5;
    }
}
