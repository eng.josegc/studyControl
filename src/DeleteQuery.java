package src;
import java.sql.*;

public class DeleteQuery {
    private static String url = "jdbc:mysql://localhost:3306/studyControl";
    private static String user = "root";
    private static String pass = "arzeus1998";

    public static void deleteTopic(int reviewId) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "DELETE FROM review WHERE review_id = ?";           
            PreparedStatement pstmt = conn.prepareStatement(query);
           
            pstmt.setInt(1, reviewId);
            pstmt.executeUpdate();
            conn.close();
            pstmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteSubject(int subjectId) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "DELETE FROM subject WHERE subject_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            
            pstmt.setInt(1, subjectId);
            pstmt.executeUpdate();
            conn.close();
            pstmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
