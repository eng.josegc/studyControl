package src;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class ManageSubject extends JFrame implements WindowListener, ActionListener {
	private Vector<JButton> editButtons, deleteButtons; 
    private Vector<JLabel> subjectNames;
    private Vector<JLabel> columnNames;
    private JScrollPane scroll;
    private JPanel panel;
    private int buttonIndex, option;
    private String subjectSelected;
    private JButton buttonPressed;

    public ManageSubject() {
        // Frame
        super("Manage Subjects");
        addWindowListener(this);

        buttonIndex = 0;
        option = 0;
        subjectSelected = "";
        buttonPressed = new JButton();

        // Panel
        panel = new JPanel(new GridBagLayout());
        scroll = new JScrollPane(panel);
        add(scroll);

        // Calls to modules
        methodController();
    }

    private void methodController() {
        initializeLabels();
        initializeButtons();
        setLabelsStyles();
        placeComponents();
        showGui();
    }

    private void initializeButtons() {
        editButtons = new Vector<JButton>();
        deleteButtons = new Vector<JButton>();

        for(int i = 0; i < (User.getSubjectNames()).size(); i++) {
            editButtons.add(GuiButton.getEditButton(20, "Edit"));
            deleteButtons.add(GuiButton.getDeleteButton(20, "Delete"));
            editButtons.get(i).addActionListener(this);
            deleteButtons.get(i).addActionListener(this);
        }
    }

    private void initializeLabels() {
        columnNames = new Vector<JLabel>();
        columnNames.add(new JLabel("Subject"));
        columnNames.add(new JLabel("Edit"));
        columnNames.add(new JLabel("Delete"));

        subjectNames = new Vector<JLabel>();
        for(String s : User.getSubjectNames()) {
            subjectNames.add(new JLabel(s));
        }
    }

    private void setLabelsStyles() {
        for(JLabel l : columnNames) {
            l.setHorizontalAlignment(JLabel.CENTER);
            l.setOpaque(true);
            l.setBackground(ColorPalette.getBase2());
            l.setForeground(ColorPalette.getBase1());
            l.setFont(Fonts.getTittleFontStyle());
        }

        for(JLabel l : subjectNames) {
            l.setHorizontalAlignment(JLabel.CENTER);
            l.setOpaque(true);
            l.setBackground(ColorPalette.getBase2());
            l.setForeground(ColorPalette.getBase1());
            l.setFont(Fonts.getTextFontStyle());
        }
    }

    private void placeComponents() {
        // Column names
        for(int i = 0; i < columnNames.size(); i++) {
            panel.add(
                columnNames.get(i),
                Constraints.setConstraints(i, 0, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                );
        }

        for(int row = 1, i = 0; i < subjectNames.size(); row++, i++) {
            panel.add(
                subjectNames.get(i),
                Constraints.setConstraints(0, row, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                );
        }
         
        for(int i = 0, row = 1; i < editButtons.size(); row++, i++) {
            panel.add(
                editButtons.get(i),
                Constraints.setConstraints(1, row, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                );

            panel.add(
                deleteButtons.get(i),
                Constraints.setConstraints(2, row, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
                );
        }
    }

    private void showGui() {
        setSize(700, 600);
        setLocationRelativeTo(null);
        setIconImage(AppIcon.getAppIcon());
        //Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
        //setSize(screenSize.width, screenSize.height/2);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if(editButtons.contains(e.getSource())) {
            // Converts the object to JButton
            buttonPressed = (JButton) e.getSource();

            // Gets the index in the Vector of this button
            buttonIndex = editButtons.indexOf(buttonPressed);

            // Gets the name of the subject
            System.out.println(buttonIndex + " pressed");            
        } else if(deleteButtons.contains(e.getSource())) {
            // Converts the object to JButton
            buttonPressed = (JButton) e.getSource();

            // Gets the index in the Vector of this button
            buttonIndex = deleteButtons.indexOf(buttonPressed);

            String dialogOptions[] = {"Yes", "No"}; 

            option = JOptionPane.showOptionDialog(
                scroll,
                "<html>All topics of this subject include it will be deleted, <br/>are you sure to delete this topic?</html>",
                "Delete subject and its topics",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                dialogOptions,
                dialogOptions[1]
            );
            
            if(option == 0) {
                DeleteQuery.deleteSubject(
                        // Returns the id that match with the name of the subject selected
                        User.searchSubjectId(subjectNames.get(buttonIndex).getText())
                );

                removeGraphicComponent(subjectNames.get(buttonIndex));
                removeGraphicComponent(editButtons.get(buttonIndex));
                removeGraphicComponent(deleteButtons.get(buttonIndex));
                MainView.removeTab(subjectNames.get(buttonIndex).getText());
            }
        }
    }

    private void removeGraphicComponent(Component c) {
        panel.remove(c);
        panel.revalidate();
        panel.repaint();
    }

    public void windowClosing(WindowEvent arg0) {
        MainView.changeSubframeOpenedState(false);
    }

    public void windowOpened(WindowEvent arg0) {}
    public void windowClosed(WindowEvent arg0) {}
    public void windowIconified(WindowEvent arg0) {}
    public void windowDeiconified(WindowEvent arg0) {}
    public void windowActivated(WindowEvent arg0) {}
    public void windowDeactivated(WindowEvent arg0) {}
}
