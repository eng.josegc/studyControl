package src;
import java.sql.*;

public class Register {
    // Util for insert new user data into DB
    public static void newUser(String name, String password, int active) {
        String url = "jdbc:mysql://localhost:3306/studyControl";
        String user = "root";
        String pass = "arzeus1998";
        String query = "INSERT INTO user (name, password, active) VALUES (?,?,?)";
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url, user, pass);

            // A precompiled statement for future queries
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            // Inserts the values of the new user into DB
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            preparedStatement.setInt(3, active);
            preparedStatement.execute();
            connection.close();
        } catch(Exception e) {
            System.err.println("Exception: " + e);
        }
    }
}
