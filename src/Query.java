/*
 *  This class contains the required queries as 
 *  a method static utils.
 * */
package src;
import java.sql.*;
import java.util.*;
import java.text.*;

public class Query {
    // Data access
    private static String url = "jdbc:mysql://localhost:3306/studyControl";
    private static String user = "root";
    private static String pass = "arzeus1998";
    
    // update
    public static void changeActiveSession(String user_name, int status) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = (status == 1) ?
                "UPDATE user SET active = 1 WHERE name = ?" : 
                "UPDATE user SET active = 0 WHERE name = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, user_name);
            pstmt.execute();

            // Closing connections
            conn.close();
            pstmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateDateReview(java.util.Date newDate, int reviewId) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE review SET review_date = ? WHERE review_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            
            // Java date to sql date
            java.sql.Date sqlDate = new java.sql.Date(newDate.getTime()); 

            // Parameters for prepared statement
            pstmt.setDate(1, sqlDate);
            pstmt.setInt(2, reviewId);

            pstmt.executeUpdate();

            // Closing connections
            conn.close();
            pstmt.close();
           
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Error while updating date");
        }
    }

    public static void updateNumberOfReview(int reviewId, int numberOfReview) {
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE review SET number_of_review = ? WHERE review_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
           
            pstmt.setInt(1, ++numberOfReview);
            pstmt.setInt(2, reviewId);
            
            pstmt.executeUpdate();

            // Closing connections
            conn.close();
            pstmt.close();
           
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
