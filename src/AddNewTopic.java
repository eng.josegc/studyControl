package src;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.text.*;

public class AddNewTopic extends JFrame implements ActionListener, WindowListener {
    protected JTextField topicName, newSubjectName;
    protected JComboBox subjects, stage, year, month, day;
    private JButton cancel, save, advanceOptions;
    private int subjectSelectedId, stageSelected;
    private String yearSelected, monthSelected, daySelected, lastStudyDate;
    protected Vector<JLabel> labels;
    private boolean newSubjectChoosed, moreOptionsStatus;

    public AddNewTopic(String windowTittle) {
        super(windowTittle);
        setLayout(new GridBagLayout());

        topicName = new JTextField();
        newSubjectName = new JTextField();


        // For know what type of query call
        newSubjectChoosed = false;

        cancel = GuiButton.getCancelButton(25, "Cancel");
        save = GuiButton.getSaveButton(25, "Save");
        advanceOptions = GuiButton.getAdvanceOptionsButton(25, "Customize");
    
        moreOptionsStatus = false;

        methodController();
    }

    private void methodController() {
        // Initialization
        loadSubjectsNames();
        fillStagesValues();
        setLastDateStudiedOptions();

        // Default values
        setDefaultStatusForMoreSettings();
        setDefaultOptions();

        // Style
        setStylesForJTextField();
        setJButtonStyles();
        setLabelsTexts();
        setLabelsStyles();
        setJComboBoxStyles();

        // Events
        setActionListeners();
        setActionForSubjectCombo();
        setActionForJTextField();
        setActionForMoreOptions();
        setActionForAddNewSubjectField();
        setActionForStageCombo();
        setActionForDateCombos();
        addWindowListener();

        // Launch 
        placeComponents();
        launchGui();
    }
    
    private void setDefaultOptions() {
        subjects.setEditable(true);
        subjects.setSelectedItem(subjects.getItemAt(0));

        stage.setEditable(true);
        stage.setSelectedItem(stage.getItemAt(0));
    }
   
    // Returns the values of year, month and day selected
    private void setActionForDateCombos() {
        // Year 
        year.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JComboBox item = (JComboBox) e.getSource(); 
                        int index = item.getSelectedIndex();
                        yearSelected = (String) year.getItemAt(index);
                    }
                }        
        );

        // Month
        month.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JComboBox item = (JComboBox) e.getSource(); 
                        int index = item.getSelectedIndex();
                        monthSelected = (String) month.getItemAt(index);
                    }
                }        
        );

        // Day
        day.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JComboBox item = (JComboBox) e.getSource(); // Object to JComboBox casting
                        int index = item.getSelectedIndex(); // Position of this item
                        daySelected = (String) day.getItemAt(index); // String value
                    }
                }        
        );
    }

    private void setValueForLastStudyDate() {
        lastStudyDate = yearSelected;
        lastStudyDate += "-";
        lastStudyDate += monthSelected;
        lastStudyDate += "-";
        lastStudyDate += daySelected;
    }

    private void setDefaultValuesForCombos() {
        // Subject
        subjects.setSelectedItem(subjects.getItemAt(0));
        stage.setSelectedItem(stage.getItemAt(0));
    }

    // Reset values for fields
    private void clearFields() {
        topicName.setText("");
        newSubjectName.setText("");
        setDefaultValuesForCombos();
    }

    private void setActionForMoreOptions() {
        advanceOptions.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if(moreOptionsStatus != true) {
                            year.setEnabled(true); 
                            month.setEnabled(true); 
                            day.setEnabled(true); 
                            stage.setEnabled(true); 
                            moreOptionsStatus = true;
                            advanceOptions.setText("Customize");
                            advanceOptions.setToolTipText("Customize");
                        } else {
                            year.setEnabled(false); 
                            month.setEnabled(false); 
                            day.setEnabled(false); 
                            stage.setEnabled(false); 
                            moreOptionsStatus = false;
                            advanceOptions.setText("Customize");
                            advanceOptions.setToolTipText("Customize");
                        }
                    }
                }
        );
    }

    // Default values for combo of last study date
    private void setLastDateStudiedOptions() {
        // YEAR --------------------------------------------------- 
        Date date = new Date();
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        String yearValue = yearFormat.format(date); 
        int yearCounter = Integer.parseInt(yearFormat.format(date));
        year = new JComboBox();

        // Fills from current year to 10 years back
        for(int y = yearCounter; y >= (yearCounter-10); y--) {
            year.addItem(String.valueOf(y));
        }

        year.setEditable(true);

        // Default value selected in JComboBox
        year.setSelectedItem(yearValue);

        // Default value for the query
        yearSelected = String.valueOf(year.getSelectedItem());

        // MONTH ------------------------------------------------- 
        month = new JComboBox();
        for(int m = 1; m < 13; m++) {
            month.addItem(String.valueOf(m));
        }
        
        month.setEditable(true);
        month.setSelectedItem(1);
        monthSelected = String.valueOf(month.getSelectedItem());

        // DAY --------------------------------------------------- 
        day = new JComboBox();
        for(int d = 1; d < 32; d++) {
            day.addItem(String.valueOf(d));
        }

        day.setEditable(true);
        day.setSelectedItem(1);
        daySelected = String.valueOf(day.getSelectedItem());
    }

    private void setLabelsStyles() {
        for(JLabel label : labels) {
            label.setFont(Fonts.getDescriptionFontStyle());
            label.setOpaque(true);
            label.setBackground(ColorPalette.getBase2());
            label.setForeground(ColorPalette.getBase1());
        }
    }

    private void setDefaultStatusForMoreSettings() {
        year.setEnabled(false); 
        month.setEnabled(false); 
        day.setEnabled(false); 
        stage.setEnabled(false); 
    }

    private void setLabelsTexts() {
        labels = new Vector<JLabel>();
        labels.add(new JLabel("Name of the new topic:"));
        labels.add(new JLabel("Subject owner:"));
        labels.add(new JLabel("Add new subject:"));
        labels.add(new JLabel("Study stage:"));
        labels.add(new JLabel("Last date when you review this topic:"));
        labels.add(new JLabel("Year"));
        labels.add(new JLabel("Month"));
        labels.add(new JLabel("Day"));
    }

    private void fillStagesValues() {
        stage = new JComboBox();
        Review.fillNumberOfReview();
        Vector stages = new Vector<String>(Review.getStagesOfStudy());
        for(int i = 0; i < stages.size(); i++) {
            stage.addItem(stages.get(i));
        }
    }

    private void setJComboBoxStyles() {
        subjects.setFont(Fonts.getTextFontStyle());
        stage.setFont(Fonts.getTextFontStyle());
        year.setFont(Fonts.getTextFontStyle());
        month.setFont(Fonts.getTextFontStyle());
        day.setFont(Fonts.getTextFontStyle());
    }

    private void setJButtonStyles() {
        save.setOpaque(true);
        save.setBackground(ColorPalette.getBase5());
        cancel.setOpaque(true);
        cancel.setBackground(ColorPalette.getBase5());
        advanceOptions.setOpaque(true);
        advanceOptions.setBackground(ColorPalette.getBase5());
    }

    private void setActionListeners() {
        cancel.addActionListener(this);
        save.addActionListener(this);
    }

    private void setStylesForJTextField() {
        topicName.setFont(Fonts.getTextFontStyle());
        newSubjectName.setFont(Fonts.getTextFontStyle());
    }

    // Retrieves subjects names of user
    private void loadSubjectsNames() {
        subjects = new JComboBox<String>();

        subjects.addItem("Choose a subject");

        for(String name : User.getSubjectNames()) {
            subjects.addItem(name);
        }
    }

    private void placeComponents() {
        // Field for input of topic name
        add(
            labels.get(0),
            Constraints.setConstraints(0, 0, 1, 0, 3, 1, new int[]{10,0,5,0}, 'h')
           );

        add(
            topicName,
            Constraints.setConstraints(0, 1, 1, 0, 3, 1, new int[]{0,0,10,0}, 'h')
           );

        // Existing subjects
        add(
            labels.get(1),
            Constraints.setConstraints(0, 2, 1, 0, 3, 1, new int[]{10,0,5,0}, 'h')
           );
        
        add(
            subjects,
            Constraints.setConstraints(0, 3, 1, 0, 3, 1, new int[]{0,0,10,0}, 'h')
           );

        // New subject
        add(
            labels.get(2),
            Constraints.setConstraints(0, 4, 1, 0, 3, 1, new int[]{10,0,5,0}, 'h')
           );

        add(
            newSubjectName,
            Constraints.setConstraints(0, 5, 1, 0, 3, 1, new int[]{0,0,5,0}, 'h')
           );


        // Stage of study
        add(
            labels.get(3),
            Constraints.setConstraints(0, 6, 1, 0, 3, 1, new int[]{10,0,5,0}, 'h')
           );

        add(
            stage,
            Constraints.setConstraints(0, 7, 1, 0, 3, 1, new int[]{0,0,10,0}, 'h')
           );

        // Date of next review
        add(
            labels.get(4),
            Constraints.setConstraints(0, 8, 1, 0, 3, 1, new int[]{10,0,5,0}, 'h')
           );

        add(
            labels.get(5),
            Constraints.setConstraints(0, 9, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );
        
        add(
            year,
            Constraints.setConstraints(0, 10, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );
        
        add(
            labels.get(6),
            Constraints.setConstraints(1, 9, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );

        add(
            month,
            Constraints.setConstraints(1, 10, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );

        add(
            labels.get(7),
            Constraints.setConstraints(2, 9, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );

        add(
            day,
            Constraints.setConstraints(2, 10, 1, 0, 1, 1, new int[]{0,0,10,0}, 'h')
           );
        
        // Cancel button
        add(
            cancel,
            Constraints.setConstraints(0, 11, 1, 0, 1, 1, new int[]{10,0,5,0}, 'h')
           );


        add(
            advanceOptions,
            Constraints.setConstraints(1, 11, 1, 0, 1, 1, new int[]{10,0,5,0}, 'h')
           );

        
        // Save button
        add(
            save,
            Constraints.setConstraints(2, 11, 1, 0, 2, 1, new int[]{10,0,5,0}, 'h')
           );
    }

    private void launchGui() {
        setSize(500, 500);
        setIconImage(AppIcon.getAppIcon());
        setResizable(false);
        setLocationRelativeTo(null);
        getContentPane().setBackground(ColorPalette.getBase3());
        setVisible(true);
    }

    private void setActionForAddNewSubjectField() {
        newSubjectName.addMouseListener(
                new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        newSubjectName.setText("");
                        subjects.setEnabled(false);
                        newSubjectChoosed = true;
                    }
                }
        );

        newSubjectName.addMouseListener(
                new MouseAdapter() {
                    public void mouseRealeased(MouseEvent e) {
                        if(newSubjectName.getText() == "") {
                            subjects.setEnabled(true);
                            newSubjectChoosed = false;
                        } else {
                            newSubjectChoosed = true;
                        }
                    }
                }
        );
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == cancel) {
            dispose();
            MainView.changeSubframeOpenedState(false);
        } else if(e.getSource() == save) {
            // Case 1: insert new topic with existing subject -------------------------------------
            if(moreOptionsStatus != true && newSubjectChoosed != true) {
                if(!((topicName.getText()).isEmpty()) && ((newSubjectName.getText()).isEmpty())) {
                    System.out.println("In case 1:");
                    if(subjects.getSelectedItem() != subjects.getItemAt(0)) {
                        // Increments by one the day of study for review the next day
                        Calendar c = Calendar.getInstance();
                        c.setTime(DateUtils.getToday());
                        c.add(Calendar.DATE, 1);
                        java.util.Date date = c.getTime();

                        // Inserts into db
                        CreateQuery.insertNewTopic(
                                topicName.getText(), 
                                subjectSelectedId,
                                date
                                );

                        // Confirmation dialog
                        JOptionPane.showMessageDialog(
                                null,
                                "Topic registered",
                                "Topic registered succeful", 
                                JOptionPane.INFORMATION_MESSAGE
                        );

                        clearFields();
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Please, choose one subject", 
                                "Fill all fields",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }
                } else if(((topicName.getText()).isEmpty()) && ((newSubjectName.getText()).isEmpty())) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Please, type the new topic name", 
                            "Fill all fields",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                }
            }  else if(newSubjectChoosed == true && moreOptionsStatus != true) {
                //Case 2: user insert new topic and new subject without edit more options ---------
                System.out.println("In case 2:");
                CreateQuery.insertNewTopic(
                        topicName.getText(),
                        newSubjectName.getText(),
                        User.getName()
                );

                JOptionPane.showMessageDialog(
                        null,
                        "Topic & subject registered succeful", 
                        "Registered",
                        JOptionPane.INFORMATION_MESSAGE
                );

                clearFields();
            } else if(moreOptionsStatus == true && newSubjectChoosed != true) {
                // Case 3: user inserts new topic of existig subject and edit more options --------
                System.out.println("In case 3:");
                if(!((topicName.getText()).isEmpty())) {
                    if(subjects.getItemAt(0) != subjects.getSelectedItem()) {
                        setValueForLastStudyDate();
                        
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                            CreateQuery.insertNewTopic(
                                topicName.getText(),
                                DateUtils.calculateNextReview(
                                    sdf.parse(lastStudyDate),
                                    stage.getSelectedIndex()    
                                    ),
                                subjectSelectedId,
                                stageSelected
                            );

                            JOptionPane.showMessageDialog(
                                    null,
                                    "Topic registered succeful", 
                                    "Registered",
                                    JOptionPane.INFORMATION_MESSAGE
                            );

                            clearFields();
                        } catch(Exception pe) {
                            System.out.println("Exception at convert String to Date in AddNewTopic: ");
                            pe.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Please, choose a subject", 
                                "Choose a subject",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }
                } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Please, type the name of topic", 
                                "Type the name",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                }
            } else if(moreOptionsStatus == true && newSubjectChoosed == true) {
                // Case 4: new subject will be added and new topic with custom options.
                System.out.println("In case 4:");
                if(!(topicName.getText()).isEmpty()) {
                    if(!(newSubjectName.getText()).isEmpty()) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        setValueForLastStudyDate();

                        try {
                            CreateQuery.insertNewTopic(
                                topicName.getText(),
                                newSubjectName.getText(),
                                User.getName(),
                                stageSelected,
                                DateUtils.calculateNextReview(
                                    sdf.parse(lastStudyDate),
                                    stage.getSelectedIndex()    
                                    )
                            );

                            JOptionPane.showMessageDialog(
                                    null,
                                    "Topic and new subject registered succeful", 
                                    "Registered",
                                    JOptionPane.INFORMATION_MESSAGE
                            );

                            clearFields();
                } catch(Exception case4Exception) {
                    System.out.println(
                            "Exception at insert new topic, case 4 -> " + 
                            case4Exception 
                            );
                    case4Exception.printStackTrace();
                }
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Please, type the name of new subject", 
                                "Type the name",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }
                } else {
                    JOptionPane.showMessageDialog(
                            null,
                            "Please, type the name of topic", 
                            "Type the name",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                }
            }
        }
    }

    private void addWindowListener() {
        addWindowListener(this);
    }

    // Set value of some window opened to false and therefore
    // lets to other windows open.
    public void windowClosing(WindowEvent arg0) {
        MainView.changeSubframeOpenedState(false);
    }

    public void windowOpened(WindowEvent arg0) {}
    public void windowClosed(WindowEvent arg0) {}
    public void windowIconified(WindowEvent arg0) {}
    public void windowDeiconified(WindowEvent arg0) {}
    public void windowActivated(WindowEvent arg0) {}
    public void windowDeactivated(WindowEvent arg0) {}

    private void setActionForSubjectCombo() {
        subjects.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        subjectSelectedId = User.searchSubjectId(
                                (String)((JComboBox)e.getSource()).getSelectedItem()
                        );

                    }
                }
        );
    }

    private void setActionForStageCombo() {
        stage.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        stageSelected = ((JComboBox)e.getSource()).getSelectedIndex();
                    }
                }
        );
    }

    public void	setActionForJTextField() {
        // Sets field of topic name empty when user clic it
        topicName.addMouseListener(
                new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        topicName.setText(""); 
                    }
                }
        );
    }
}
