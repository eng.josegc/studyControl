package src;
import java.sql.*;

// Checks the information in database
public class Login {
    // Method for evaluates the authentication credentials of user
    public static boolean authenticate(String userName, String password) {
        try {
            if(ReadQuery.checkIfExistsUser(userName, password) == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
} 
