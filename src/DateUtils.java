package src;
import java.util.*;
import java.text.*;
import java.awt.*;
import javax.swing.*;

public class DateUtils {
    private static Calendar calendar;
    private static SimpleDateFormat dateFormat;
    private static JLabel result;

/*     public static JLabel getTodayLabel() {
        // Format for shows date
        dateFormat = new SimpleDateFormat("yyyy-MMM-dd");

        // Format of gregorian calendar
        calendar = new GregorianCalendar();

        // String with the current date of gregorian calendar
        result = new JLabel(dateFormat.format(calendar.getTime()));
        result.setFont(Fonts.getTittleFontStyle());
        result.setForeground(ColorPalette.getDark());
        result.setHorizontalAlignment(JLabel.CENTER);
        return result;
    } */

    public static Date getToday() {
        Date date = new Date(); // Date for get the current time

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            date = dateFormat.parse(dateFormat.format(date));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }

        return date; // Formats the current time with the given format
    }

    public static Date setStandardFormat(java.util.Date date) {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            date = dateFormat.parse(dateFormat.format(date));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }

        return date;
    }

    /*
     * When complete button is pressed this function
     * calculate the new date for the review using the number
     * of review and the current date.
     * 
     * @param date Date to be recalculate.
     * @param numberOfReview How many times this topic has been studied.
     * **/
    public static Date calculateNextReview(java.util.Date date, int numberOfReview) {
        Calendar c = null;

        c = Calendar.getInstance();
        c.setTime(date); // Receives the date parameter

        switch(numberOfReview) {
            case 0:
                c.add(Calendar.DATE, 2);
                break;
            case 1:
                c.add(Calendar.DATE, 3);
                break;
            case 2:
                c.add(Calendar.DATE, 8);
                break;
            case 3:
                c.add(Calendar.DATE, 15);
                break;
            case 4:
                c.add(Calendar.DATE, 31);
                break;
            case 5:
                c.add(Calendar.DATE, 61);
                break;
            case 6:
                c.add(Calendar.DATE, 121);
                break;
            case 7:
                c.add(Calendar.DATE, 181);
                break;
        }

        return date = c.getTime(); // Assigns the new date calculated to date
    }
}
