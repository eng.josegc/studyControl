package src;
import java.awt.*;
import javax.swing.*;

public class GuiButton {
    /**
     *  Generates a JButton with icon and tooltip.
     *
     *  @param s size to be set as weight and height
     *  @param t tool tip for this button
     * */

    public static JButton getOptionButton(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/option.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getAddSubject(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/medium/subject.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getAdvanceOptionsButton(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/advance_options.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getCancelButton(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/cancel.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getSaveButton(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/save.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getNormalButton(String t) {
        JButton button = new JButton(t);
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getAddButton(int s, String t) {
        JButton button = new JButton(t, ResizeImage.createImage("./pictures/add.png", s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }

    public static JButton getDeleteAllButton(int s, String t) {
        String path = "./pictures/delete_all.png";
        JButton button = new JButton(ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getCompleteButton(int s, String t) {
        String path = "./pictures/complete.png";
        JButton button = new JButton(t, ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setBackground(ColorPalette.getBase5());
        return button;
    }

    public static JButton getEditButton(int s, String t) {
        String path = "./pictures/edit.png";
        JButton button = new JButton(t, ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setBackground(ColorPalette.getBase5());
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getDeleteButton(int s, String t) {
        String path = "./pictures/delete.png";
        JButton button = new JButton(t, ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setBackground(ColorPalette.getBase5());
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getPreferencesButton(int s, String t) {
        String path = "./pictures/preferences.png";
        JButton button = new JButton(ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getFlipViewButton(int s, String t) {
        String path = "./pictures/flip.png";
        JButton button = new JButton(ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getDoneButton(int s, String t) {
        String path = "./pictures/done.png";
        JButton button = new JButton(t, ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return button;
    }

    public static JButton getBackButton(int s, String t) {
        String path = "./pictures/back.png";
        JButton button = new JButton(t, ResizeImage.createImage(path, s, s));
        button.setToolTipText(t);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        button.setFont(Fonts.getTextFontStyle());
        return button;
    }
}
