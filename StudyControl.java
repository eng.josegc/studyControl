import src.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class StudyControl {
	public static void main(String args[]) {
        if(ReadQuery.findUserActive() == 1) {
			// Retrieve the user information of last session
			ReadQuery.getUser();
			new MainView();
		} else {
            new Dialogs().showDialog("Login");
		}
	}
}
